# APK Persistent Reverse Shell #

![screenshot.140.jpg](https://bitbucket.org/repo/BRXkGA/images/3404118454-screenshot.140.jpg)

Hello everyone!
This repository is an open source project for creating a proof-of-concept persistent reverse shell on Android platforms.

### What do you mean persistent? ###
While other tools for rev-shell are active for the activity itself (And sometimes as service in the background), this project uses the BOOT-COMPLETE permission to know when the OS finished rebooting. Therefore, we're able to start the reverse shell service **even after rebooting the device**. The only reason that the app will not be persistent is, of course, removing the application.

### How do you know how to start the server after OS boot? ###
I've created a **broadcast receiver** , which listens for BOOT-COMPLETE event. When that happens, the handler of the broadcast receiver just calls our service.

### What else? ###
* Building the entire reverse shell engine (Not hard.. just sockets)
* Removing unnecessery things, as the default activity
* Checking detection by mobile anti-viruses
* Bypassing those ;)

**THANK YOU**